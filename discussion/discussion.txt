What is VCS or Version Control System


	VCS is a piece of  software that manages or controls the revisions of a source code, document, collection or information in a folder or repository. This will allow us to create versions of our files.

What is Git?

	Git is an open source version control system that we use to track changes in a file within a repository. 

GitLab vs Github
	 There is not much difference between the two. Both are just competing cloud services that store and manage our online repositories

Repositories

	Local Repository - folders that use git technology. Therefore, it allows us to track changes made in the files with the folder. These changes can then be uploaded and update the files in our Remote Repositories

	Remote Repository - these are folders that use git technology but instead located in the Internet or in cloud services such as GitLab and GitHub

What is SSH key? 

	SSH or Secure Shell Key are tools we use to authenticate the uploading or of other tasks when manipulating or using git repositories. It allows us to push/upload changes to our repos without the need for passwords


Basic Git Commands for pushing for the first time:
git init - initialize a new local git repository

git add . - allow us to add the changes from our files and ready it to create a version of our files.

git commit -m "commit message" - allows us to create a new version of our files to be pushed or uploaded to our remote git repository

git remote add origin <gitURL> - allows to connect our local repository to an online/remote repository. "origin" is the default "alias" or another name for your remote connection

git push origin master - allows us to push/upload the new version/commit we created into our online repository

Other Git Commands:
git remote -v - allow us to check the remote connections of our local repo
git status - allows to check for files that are not yet added or committed s2